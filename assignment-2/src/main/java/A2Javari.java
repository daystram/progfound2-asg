import java.util.ArrayList;
import java.util.Scanner;

import animals.Animal;
import cages.Arranger;
import cages.Cage;
import cages.IndoorCage;
import cages.OutdoorCage;

public class A2Javari {

    // define variables
    private static String[] animalType = {"cat", "lion", "eagle", "parrot", "hamster"};
    private static ArrayList[][] animalCages = new ArrayList[5][3];

    // main() method
    public static void main(String[] args) {
        System.out.println("Welcome to Javari Park!\nInput the number of animals");
        Scanner input = new Scanner(System.in);

        // ask user for info of every animal types
        for (int i = 0; i < 5; i++) {
            int amount;
            // prompt user for valid animal amount input
            while (true) {
                try {
                    System.out.print(animalType[i] + ": ");
                    amount = Integer.parseInt(input.nextLine());
                    break;
                } catch (Exception e) {
                    System.out.println("Please input a number!");
                }
            }

            // skip if no animals for that type
            if (amount == 0) {
                Animal.setAnimalAmount(0, i);
                continue;
            }

            // prompt user for valid animal data input
            while (true) {
                try {
                    System.out.println("Provide the information of " + animalType[i] + "(s):");
                    String[][] data;
                    String line = input.nextLine();

                    // count the number of given data pairs
                    int sepAmount = line.length() - line.replace("|", "").length();
                    if (sepAmount != amount) {
                        // given data does not match animal amount
                        System.out.println("Please provide the correct amount of "
                                            + animalType[i] + " data!");
                    } else {
                        // correct amount of pairs, try parsing given data
                        data = parseData(line);
                        // pack animals into unordered cages, temporary storage to [0]
                        animalCages[i][0] = packCage(Animal.parseAnimal(data, i), i);
                        animalCages[i] = Arranger.splitThree(animalCages[i][0], amount);
                        break;
                    }
                } catch (Exception e) {
                    // given data uses wrong format
                    System.out.println("Incorrect format! Use <name>|<length>,...");
                }
            }

        }
        System.out.println("Animals have been successfully recorded!\n");
        System.out.println("=============================================");
        System.out.println("Cage arrangement:");

        // show cage arrangement
        for (int i = 0; i < 5; i++) {
            // no animals of the given type, do not show cage arrangement
            if (Animal.getAnimalAmount(i) == 0) {
                continue;
            }

            // show cage position
            if (i == 1 | i == 2) {
                System.out.println("location: outdoor");    // lion & eagle are outdoor
            } else {
                System.out.println("location: indoor");     // remaining are indoor
            }

            // show cages before and after rearranging
            Cage.printCageStack(animalCages[i]);
            System.out.println("After rearrangement...");
            animalCages[i] = Arranger.arrange(animalCages[i]);  // rearrange cage order
            Cage.printCageStack(animalCages[i]);
        }

        // show the total number of animals for every type
        System.out.println("NUMBER OF ANIMALS:");
        for (int i = 0; i < 5; i++) {
            System.out.println(animalType[i] + ":" + Animal.getAnimalAmount(i));
        }
        System.out.print("\n=============================================");

        // keep prompting user for which animal to visit
        while (true) {
            // prompt user for animal type to visit
            System.out.println("\nWhich animal you want to visit?\n"
                                + "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            int type;
            try {
                type = Integer.parseInt(input.nextLine());
            } catch (Exception e) {
                System.out.println("Please input a number!");
                continue;   // non integer given
            }
            if (type == 99) {
                break;  // exit command, leave loop
            } else if (type < 1 || type > 5) {
                System.out.println("Command not found!");
                continue;   // command not found
            }

            // standardise the animal type indexing
            type = standardIndex(type);

            // check if animal type is available in park
            if (Animal.getAnimalAmount(type) == 0) {
                System.out.println("Sorry, we do not have " + animalType[type]
                                    + "s in the park! Back to the office!");
                continue;   // animal type unavailable
            }

            // ask for animal name, check if available
            System.out.print("Mention the name of " + animalType[type] + " you want to visit: ");
            Animal selected = available(input.nextLine(), type);  // get animal, if available
            if (selected == null) {
                System.out.println("There is no " + animalType[type]
                                    + " with that name! Back to the office!");
                continue;   // animal with given name unavailable
            }

            // animal found, show available commands
            System.out.println("You are visiting " + selected.getName() + " (" + animalType[type]
                                + ") now, what would you like to do?");
            Animal.printCommand(selected);

            // instruct animals based on user command
            selected.instruct(Integer.parseInt(input.nextLine()));

            System.out.println("Back to the office!");
        }

        // leave park; exit code
        System.out.println("Thank you for visiting Javari Park!");
        input.close();
    }

    // parse given data into 2D Array of data pairs
    private static String[][] parseData(String line) {
        // split pairs by ','
        String[] pairs = line.split(",");
        String[][] data = new String[pairs.length][2];
        for (int i = 0; i < pairs.length; i++) {
            data[i] = pairs[i].split("\\|");
        }
        return data;

    }

    // pack the animals into their cages
    private static ArrayList<Cage> packCage(ArrayList animals, int type) {
        ArrayList<Cage> cages = new ArrayList<>();
        if (type == 1 || type == 2) {       // wild animals go outdoor
            // pack outdoor
            for (Object animal : animals) {
                cages.add(new OutdoorCage((Animal) animal, ((Animal) animal).getLength()));
            }
        } else {
            // pack indoor
            for (Object animal : animals) {
                cages.add(new IndoorCage((Animal) animal, ((Animal) animal).getLength()));
            }
        }
        return cages;
    }

    // standardise animal type indexing
    private static int standardIndex(int type) {
        switch (type) {
            case 1: return 0;
            case 3: return 4;
            case 4: return 3;
            case 5: return 1;
            default: return type;
        }
    }

    // check if animal is in park
    private static Animal available(String name, int type) {
        // loop through every animal in the cageStack
        for (int i = 0; i < 3; i++) {
            ArrayList<Cage> rowStack = animalCages[type][i];
            for (Cage cage : rowStack) {
                if (cage.getAnimal().getName().equals(name)) {
                    // matching name found, return animal
                    return cage.getAnimal();
                }
            }
        }
        // no matching name found
        return null;
    }
}
