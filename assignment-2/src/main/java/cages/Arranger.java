package cages;

import java.util.ArrayList;

public class Arranger {

    // rearrange cageStack
    public static ArrayList[] arrange(ArrayList<Cage>[] animalCages) {
        ArrayList[] finalCage = new ArrayList[3];
        // cycle through rows
        for (int i = 0; i < 3; i++) {
            ArrayList finalStack = new ArrayList<Cage>();
            // flip the order of cages in every row
            for (Cage cage : animalCages[i]) {
                finalStack.add(0, cage);
            }

            // reorder the rows arrangement
            if (i == 2) {
                finalCage[0] = finalStack;
            } else {
                finalCage[i + 1] = finalStack;
            }
        }
        return finalCage;
    }

    // place animal cages to 3 rows evenly
    public static ArrayList[] splitThree(ArrayList animalCages, int quantity) {
        int minRow = (quantity / 3) + ((quantity % 3) / 2);     // amount of cages in row 1 & 2
        ArrayList[] cageStack = {new ArrayList(), new ArrayList(), new ArrayList()};
        // cycle through rows
        for (int i = 0; i < 3; i++) {
            // iterate though the cages for every row, place cage into row, if still available
            for (int j = 0; j < minRow; j++) {
                if (!animalCages.isEmpty()) {
                    cageStack[i].add(j, animalCages.get(0));
                    animalCages.remove(0);      // remove placed cage from initial stack
                }
            }
        }

        // place remaining cages to cageStack, if still available
        if (quantity % 3 == 1) {
            if (cageStack[0].isEmpty()) {
                // only 1 cage in initial stack, place on lowest row
                cageStack[0].add(animalCages.get(0));
            } else {
                // place on top row
                cageStack[2].add(animalCages.get(0));
            }
        }
        return cageStack;
    }
}
