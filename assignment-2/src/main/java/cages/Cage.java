package cages;

import java.util.ArrayList;

import animals.Animal;

public class Cage {

    // define variables
    private Animal animal;
    private char type;
    protected int length;
    private int width;

    // Cage constructor
    public Cage(Animal animal) {
        this.animal = animal;
    }

    // show the stack of cages
    public static void printCageStack(ArrayList[] cageStack) {
        StringBuilder output = new StringBuilder();     // used for easier string composition
        // loop through every row
        for (int i = 0; i < 3; i++) {
            output.append("level ").append(3 - i).append(": ");     // show row number
            // loop though cages in every row
            for (int j = 0; j < cageStack[2 - i].size(); j++) {
                Animal animal = ((Cage) cageStack[2 - i].get(j)).getAnimal();
                // compose the cage info format
                output.append(animal.getName()).append(" (").append(animal.getLength())
                                .append(" - ").append(((Cage) cageStack[2 - i]
                                .get(j)).getType()).append("), ");
            }
            output.append("\n");
        }
        // print the info
        System.out.println(output);
    }

    // getters and setters
    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getArea() {
        return width * length;
    }
}
