package cages;

import animals.Animal;

public class IndoorCage extends Cage {

    // IndoorCage constructor
    public IndoorCage(Animal animal, int length) {
        super(animal);
        char type;
        // categorise cage based on animal length
        if (length < 45) {
            type = 'A';
            this.length = 60;
        } else if (length > 60) {
            type = 'C';
            this.length = 90;
        } else {
            type = 'B';
            this.length = 120;
        }
        super.setType(type);
        super.setWidth(60);
    }
}
