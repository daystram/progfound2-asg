package cages;

import animals.Animal;

public class OutdoorCage extends Cage {

    // OutdoorCage constructor
    public OutdoorCage(Animal animal, int length) {
        super(animal);
        char type;
        // categorise cage based on animal length
        if (length < 75) {
            type = 'A';
            super.setLength(120);
        } else if (length > 90) {
            type = 'C';
            super.setLength(150);
        } else {
            type = 'B';
            super.setLength(180);
        }
        super.setType(type);
        super.setWidth(120);
    }
}
