package animals;

import java.util.ArrayList;

public class Hamster extends Animal {

    // Hamster constructor
    public Hamster(String name, int length) {
        super(name, length);
    }

    // parse a 2D String Array into an ArrayList of Hamster(s)
    static ArrayList parseHamster(String[][] data) {
        ArrayList<Hamster> hamsters = new ArrayList<>();
        for (String[] pair : data) {
            // create a Hamster object for every data pair
            hamsters.add(new Hamster(pair[0], Integer.parseInt(pair[1])));
        }
        return hamsters;
    }

    // commands for hamsters
    public void instruct(int cmd) {
        switch (cmd) {
            case 1:
                System.out.println(this.name + " makes a voice: ngkkrit.. ngkkrrriiit");
                break;

            case 2:
                System.out.println(this.name + " makes a voice: trrr…. trrr...");
                break;

            default:
                System.out.println("You do nothing...");
        }
    }

    // show available commands
    static void printCommand() {
        System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
    }
}
