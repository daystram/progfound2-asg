package animals;

import java.util.ArrayList;

public class Eagle extends Animal {

    // Eagle constructor
    public Eagle(String name, int length) {
        super(name, length);
    }

    // parse a 2D String Array into an ArrayList of Eagle(s)
    static ArrayList parseEagle(String[][] data) {
        ArrayList<Eagle> eagles = new ArrayList<>();
        for (String[] pair : data) {
            // create an Eagle object for every data pair
            eagles.add(new Eagle(pair[0], Integer.parseInt(pair[1])));
        }
        return eagles;
    }

    // commands for eagles
    public void instruct(int cmd) {
        switch (cmd) {
            case 1:
                System.out.println(this.name + " makes a voice: kwaakk…\n"
                                    + "You hurt!");
                break;

            default:
                System.out.println("You do nothing...");
        }
    }

    // show available commands
    static void printCommand() {
        System.out.println("1: Order to fly");
    }
}
