package animals;

import java.util.ArrayList;

public class Lion extends Animal {

    // Lion constructor
    public Lion(String name, int length) {
        super(name, length);
    }

    // parse a 2D String Array into an ArrayList of Lion(s)
    static ArrayList parseLion(String[][] data) {
        ArrayList<Lion> lions = new ArrayList<>();
        for (String[] pair : data) {
            // create Lion object for every data pair
            lions.add(new Lion(pair[0], Integer.parseInt(pair[1])));
        }
        return lions;
    }

    // commands for lions
    public void instruct(int cmd) {
        switch (cmd) {
            case 1:
                System.out.println("Lion is hunting..\n"
                                    + this.name + " makes a voice: err...!");
                break;

            case 2:
                System.out.println("Clean the lion’s mane..\n"
                                    + this.name + " makes a voice: Hauhhmm!");
                break;

            case 3:
                System.out.println(this.name + " makes a voice: HAUHHMM!");
                break;

            default:
                System.out.println("You do nothing...");
        }
    }

    // show available commands
    static void printCommand() {
        System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
    }
}
