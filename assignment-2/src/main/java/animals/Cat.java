package animals;

import java.util.ArrayList;
import java.util.Random;

public class Cat extends Animal {

    // Cat constructor
    public Cat(String name, int length) {
        super(name, length);
    }

    // parse a 2D String Array into an ArrayList of Cat(s)
    static ArrayList parseCat(String[][] data) {
        ArrayList<Cat> cats = new ArrayList<>();
        for (String[] pair : data) {
            // create Cat object for every data pair
            cats.add(new Cat(pair[0], Integer.parseInt(pair[1])));
        }
        return cats;
    }

    // commands for cats
    public void instruct(int cmd) {
        switch (cmd) {
            case 1:
                System.out.println("Time to clean " + this.name + "'s fur\n"
                                    + this.name + " makes a voice: Nyaaan...");
                break;

            case 2:
                Random rand = new Random(); // use random integer to select random noises
                String[] noises = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
                System.out.println(this.name + " makes a voice: " + noises[rand.nextInt(4)]);
                break;

            default:
                System.out.println("You do nothing...");
        }
    }

    // show available commands
    static void printCommand() {
        System.out.println("1: Brush the fur 2: Cuddle");
    }
}
