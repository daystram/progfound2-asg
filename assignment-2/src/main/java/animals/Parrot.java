package animals;

import java.util.ArrayList;
import java.util.Scanner;

public class Parrot extends Animal {

    // define instance variables
    private Scanner input;

    // Parrot constructor
    public Parrot(String name, int length) {
        super(name, length);
        input = new Scanner(System.in);
    }

    // parse a 2D String Array into an ArrayList of Parrot(s)
    static ArrayList parseParrot(String[][] data) {
        ArrayList<Parrot> parrots = new ArrayList<>();
        for (String[] pair : data) {
            // create a Parrot object for every data pair
            parrots.add(new Parrot(pair[0], Integer.parseInt(pair[1])));
        }
        return parrots;
    }

    // commands for parrots
    public void instruct(int cmd) {
        switch (cmd) {
            case 1:
                System.out.println("Parrot " + this.name + " flies!\n"
                                    + this.name + " makes a voice: FLYYYY…..");
                break;

            case 2:
                // get user input, then print out user input in uppercase
                System.out.print("You say: ");
                String output = input.nextLine().trim().toUpperCase();
                if (output.isEmpty()) {
                    output = "HM?";     // say HM? if visitor did not say anything
                }
                System.out.println(this.name + " says: " + output);
                break;

            default:
                System.out.println(this.name + " says: HM?");
        }
    }

    // show available commands
    static void printCommand() {
        System.out.println("1: Order to fly 2: Do conversation");
    }
}
