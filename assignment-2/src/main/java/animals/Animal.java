package animals;

import java.util.ArrayList;

public class Animal {

    // define variables
    protected String name;
    private int length;
    private static Integer[] animalAmount = new Integer[5];     // amount of animals per type

    // Animal constructor
    public Animal(String name, int length) {
        this.name = name;
        this.length = length;
    }

    // animal parser
    public static ArrayList parseAnimal(String[][] data, int type) {
        animalAmount[type] = data.length;   // store the amount of animals for corresponding type
        // parse animal based on type
        switch (type) {
            case 0: return Cat.parseCat(data);
            case 1: return Lion.parseLion(data);
            case 2: return Eagle.parseEagle(data);
            case 3: return Parrot.parseParrot(data);
            case 4: return Hamster.parseHamster(data);
            default: return null;
        }
    }

    // show available commands of animal
    public static void printCommand(Animal animal) {
        // show animal commands based on type
        if (animal instanceof Cat) {
            Cat.printCommand();
        } else if (animal instanceof Lion) {
            Lion.printCommand();
        } else if (animal instanceof Eagle) {
            Eagle.printCommand();
        } else if (animal instanceof Parrot) {
            Parrot.printCommand();
        } else if (animal instanceof Hamster) {
            Hamster.printCommand();
        }
    }

    // instruct animal with commands
    public void instruct(int cmd) {
        // instruct the animal based on its type
        if (this instanceof Cat) {
            ((Cat) this).instruct(cmd);
        } else if (this instanceof Lion) {
            ((Lion) this).instruct(cmd);
        } else if (this instanceof Eagle) {
            ((Eagle) this).instruct(cmd);
        } else if (this instanceof Parrot) {
            ((Parrot) this).instruct(cmd);
        } else if (this instanceof Hamster) {
            ((Hamster) this).instruct(cmd);
        }
    }

    // getters and setters
    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public static int getAnimalAmount(int type) {
        return animalAmount[type];
    }

    public static void setAnimalAmount(int amount, int type) {
        animalAmount[type] = amount;
    }
}
