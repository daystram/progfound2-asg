import animals.Animal;
import animals.sections.Section;
import attractions.Attraction;
import attractions.SelectedAttraction;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import reader.CsvReader;
import visitor.Visitor;
import writer.RegistrationWriter;

/**
 * This main class contains and runs the main program.
 */
public class A3Festival {

    private static Scanner input;               // Scanner input
    private static Section[] sections;          // parsed Sections
    private static Attraction[] attractions;    // parsed Attractions
    private static Animal[] animals;            // parsed Animals

    /**
     * This main function runs the main program.
     */
    public static void main(String[] args) {
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        input = new Scanner(System.in);     // create new Scanner input

        // create new file reader, then populate system data from CSV files
        CsvReader fileReader = new CsvReader();
        System.out.println(" System is populating data...\n");
        sections = fileReader.readSection();
        animals = fileReader.readRecord();
        attractions = fileReader.readAttraction(animals);

        // show amount of valid parsed lines
        fileReader.showValidAmount();

        System.out.println("\nWelcome to Javari Park Festival - Registration Service!");

        // create an ArrayList of to be registered visitors
        ArrayList<Visitor> visitors = new ArrayList<>();
        Visitor newVisit = new Visitor();
        visitors.add(newVisit);

        // begin main program loop
        boolean running = true;
        while (running) {
            System.out.println("\nPlease answer the questions by typing the number."
                    + " Type # if you want to return to the previous menu");

            // begin info collection subroutine
            byte step = 0;
            while (step < 4) {
                // show different menu based on the current subroutine step
                switch (step) {
                    case 0:
                        // ask for section
                        step = askSection(newVisit, step);
                        break;

                    case 1:
                        // ask for animal type
                        step = askType(newVisit, step);
                        break;

                    case 2:
                        // ask for attraction
                        step = askAttr(newVisit, step);
                        break;

                    case 3:
                        // verify data
                        step = askVerify(newVisit, step);
                        break;

                    default:
                        break;
                }

            }
            // ask if user want to register to another attraction
            running = askRepeat();
        }

        // finish program by writing JSON files
        writeJson(visitors);
        input.close();      // close Scanner input
    }

    // a function to ask user which section to choose
    private static byte askSection(Visitor newVisit, byte step) {
        // show a list of available sections
        System.out.println("\nJavari Park has " + sections.length + " sections:");
        for (int i = 0; i < sections.length; i++) {
            System.out.println((i + 1) + ". " + sections[i].getName());
        }
        // try getting the user input
        while (true) {
            System.out.print("Please choose your preferred section (type the number): ");
            String command = input.nextLine();
            if (command.equals("#")) {
                // already at first menu
                System.out.println("\nYou are at the first menu!");
                break;
            } else {
                try {
                    // select the section chosen
                    newVisit.setSection(sections[Integer.parseInt(command) - 1]);
                    step++;     // proceed to next step
                    break;
                } catch (Exception e) {
                    inputHandler(e);    // handle invalid inputs
                }
            }
        }
        return step;    // return modified step
    }

    // a function to ask user which animal type to choose
    private static byte askType(Visitor newVisit, byte step) {
        // show a list of animal types in the chosen section
        System.out.println("\n--" + newVisit.getSection().getName() + "--");
        ArrayList<String> types = newVisit.getSection().getTypes(); // get the animal types
        for (int i = 0; i < types.size(); i++) {
            System.out.println((i + 1) + ". " + types.get(i));
        }
        // try getting the user input
        while (true) {
            System.out.print("Please choose your preferred animals: ");
            String command = input.nextLine();
            if (command.equals("#")) {
                step--;     // goto previous menu
                break;
            } else {
                try {
                    // select the animal type chosen
                    newVisit.setSelType(types.get(Integer.parseInt(command) - 1));
                    // collect the legible animals
                    if (getLegibleAnim(newVisit.getSelType()).isEmpty()) {
                        // no animal of selected type is legible to perform
                        System.out.println("\nUnfortunately, no "
                                + newVisit.getSelType().toLowerCase()
                                + " can perform any attraction, please choose other animals");
                    } else {
                        step++;     // proceed to next step
                    }
                    break;
                } catch (Exception e) {
                    inputHandler(e);    // handle invalid inputs
                }
            }
        }
        return step;    // return modified step
    }

    // a function to ask user which attraction to choose
    private static byte askAttr(Visitor newVisit, byte step) {
        // show a list of attractions which shows the selected animal type
        System.out.println("\n---" + newVisit.getSelType() + "---");
        System.out.println("Attractions by " + newVisit.getSelType() + ":");
        ArrayList<Attraction> availAttr = getAvailAttr(newVisit.getSelType());  // get attractions
        for (int i = 0; i < availAttr.size(); i++) {
            System.out.println((i + 1) + ". " + availAttr.get(i).getName());
        }
        // try getting the user input
        while (true) {
            System.out.print("Please choose your preferred attractions (type the number): ");
            String command = input.nextLine();
            if (command.equals("#")) {
                step--;     // goto previous step
                break;
            } else {
                try {
                    SelectedAttraction selected = availAttr.get(Integer.parseInt(command) - 1);
                    selected.setType(newVisit.getSelType());    // set the selected animal type
                    newVisit.setAttr(selected);     // select the attraction chosen
                    step++;     // proceed to next step
                    break;
                } catch (Exception e) {
                    inputHandler(e);    // handle invalid inputs
                }
            }
        }
        return step;    // return modified step
    }

    // a function to ask user to verify collected data
    private static byte askVerify(Visitor newVisit, byte step) {
        String customerName;
        System.out.println("\nWow, one more step,");

        // check if user has already been registered
        if (newVisit.getName() == null) {
            while (true) {
                System.out.print("please let us know your name: ");
                customerName = input.nextLine();
                if (customerName.isEmpty()) {
                    System.out.println("Please enter your name!\n");    // empty input given
                    continue;
                }
                newVisit.setName(customerName);     // set the new costumer's name
                break;
            }
        }
        // show selected choices
        System.out.println("\nYeay, final check!\n"
                + "Here is your data, and the attraction you chose:");
        System.out.println("Name: " + newVisit.getName());
        System.out.print("Attractions: " + newVisit.getAttr().getName()
                + " -> " + newVisit.getSelType() + "\nWith: ");
        List<Animal> performers = newVisit.getAttr().getPerformers();   // get animals performing
        for (int i = 0; i < performers.size(); i++) {       // show the name of the performers
            System.out.print(performers.get(i).getName());
            if (i != performers.size() - 1) {
                System.out.print(", ");
            }
        }
        // try getting the user input
        verify:
        while (true) {
            System.out.print("\n\nIs the data correct? (Y/N): ");
            String command = input.nextLine();
            switch (command.toLowerCase()) {
                case "y":
                    step++;     // done collecting info
                    newVisit.setAttrFinal();
                    break verify;
                case "n":
                    step = 0;   // reset steps
                    break verify;
                default:
                    System.out.print("Please enter Y or N !");
            }

        }
        return step;    // return modified step
    }

    // a function to ask user if want to register to another attraction
    private static boolean askRepeat() {
        // try getting user input
        while (true) {
            System.out.print("\nThank you for your interest. "
                    + "Would you like to register to other attractions? (Y/N): ");
            String command = input.nextLine();
            switch (command.toLowerCase()) {
                case "y":
                    return true;    // keep the main loop running
                case "n":
                    return false;    // end the main program loop
                default:
                    System.out.println("Please enter Y or N !");
            }
        }
    }

    // a function to write all the registered users' registration data to JSON
    private static void writeJson(ArrayList<Visitor> visitors) {
        System.out.print("\n... End of program, write to ");
        // try creating JSON files for every registered visitor
        for (int i = 0; i < visitors.size(); i++) {
            try {
                RegistrationWriter.writeJson(visitors.get(i));  // create JSON file
            } catch (Exception e) {
                System.out.println("An exception has occurred!");
                System.exit(0);     // exit upon uncaught exception
            }
            if (i != visitors.size() - 1) {
                System.out.print(", ");
            }
        }
    }

    // a function to handle exceptions during the step subroutines
    private static void inputHandler(Exception e) {
        if (e instanceof NumberFormatException) {
            System.out.println("Please enter a number!\n"); // non number given
        } else if (e instanceof IndexOutOfBoundsException) {
            System.out.println("Please choose a number from the list!\n");  // number not from list
        } else {
            System.out.println("An error has occurred!");
            System.exit(0);     // exit upon uncaught exception
        }
    }

    // a function to get all animals legible to perform with the given type
    private static ArrayList<Animal> getLegibleAnim(String type) {
        ArrayList<Animal> legibleAnim = new ArrayList<>();      // temporary storage
        // iterate through all registered animals
        for (Animal animal : animals) {
            if (animal.getType().equals(type) && animal.isLegible()) {
                legibleAnim.add(animal);    // animal of matching type is legible, add to list
            }
        }
        return legibleAnim;     // return the legible animals
    }

    // a function to get the attraction which shows the given animal type
    private static ArrayList<Attraction> getAvailAttr(String type) {
        ArrayList<Attraction> availAttr = new ArrayList<>();    // temporary storage
        // iterate though all attractions
        for (Attraction attr : attractions) {
            if (attr.hasAllowedType(type)) {
                availAttr.add(attr);    // attraction shows the given animal type, add to list
            }
        }
        return availAttr;   // return the available attractions
    }
}
