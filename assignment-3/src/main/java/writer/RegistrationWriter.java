package writer;

import animals.Animal;
import attractions.SelectedAttraction;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.json.JSONWriter;
import visitor.Visitor;

/**
 * This class handles saving data of registered attractions into JSON file.
 *
 * @author Programming Foundations 2 Teaching Team
 */
public class RegistrationWriter {

    // default output file name
    private static final String DEFAULT_FILENAME_FMT =
            "registration_%s.json";

    // default output file directory
    private static final String DEFAULT_DIR = "./assignment-3/registration";

    /**
     * Writes visitor's registration data into JSON file.
     * This function uses the default path provided in this class.
     *
     * @param registration  instance of registration that contains attractions
     *                      that will be watched by visitor
     * @throws IOException  when JSON file cannot be written
     */
    public static void writeJson(Visitor registration) throws IOException {
        // create output file directory if does not exist
        File output = new File(DEFAULT_DIR);
        output.mkdirs();
        String fileName = String.format(DEFAULT_FILENAME_FMT,
                registration.getName());
        String jsonFileName = fileName.replace(" ", "_");
        Path jsonFile = Paths.get(DEFAULT_DIR).resolve(jsonFileName);
        BufferedWriter fileWriter = Files.newBufferedWriter(jsonFile,
                StandardCharsets.UTF_8);
        final JSONWriter writer = new JSONWriter(fileWriter);

        // Note: JSONWriter is following Builder design pattern
        // to provide a "fluent-style" JSON file creation
        writer.array();
        buildRegistration(writer, registration);
        writer.endArray();

        System.out.print(jsonFile.getFileName().toString());

        // Ensure JSON text are flushed out from internal buffer
        // into the actual file
        fileWriter.flush();
        fileWriter.close();
    }

    /**
     * Uses call-by-reference to write content of registration using
     * given instance of JsonWriter.
     *
     * @param writer        an instance of JSONWriter passed by reference
     * @param registration  registration object
     */
    private static void buildRegistration(JSONWriter writer,
                                          Visitor registration) {
        writer.object()
                .key("registration_id").value(registration.getId())
                .key("name").value(registration.getName())
                .key("attractions").array();

        registration.getFinalAttr().forEach(attraction ->
                buildAttraction(writer, attraction)
        );

        writer.endArray().endObject();
    }

    /**
     * Uses call-by-reference to write an instance of selected attraction into
     * intermediate result of JSON file writing.
     *
     * @param writer        instance of JSONWriter containing intermediate result
     * @param attraction    an attraction that will be watched by visitor according
     *                      to the registration info
     */
    private static void buildAttraction(JSONWriter writer,
                                        SelectedAttraction attraction) {
        writer.object()
                .key("name").value(attraction.getName())
                .key("type").value(attraction.getType());

        buildPerformers(writer,
                attraction.getPerformers());

        writer.endObject();
    }

    /**
     * Uses call-by-reference to write list of animal performers into
     * intermediate result of JSON writing.
     *
     * @param writer    instance of JSONWriter containing intermediate result
     *                  of JSON writing
     * @param animals   list of animals that perform in the attraction
     */
    private static void buildPerformers(JSONWriter writer,
                                        List<Animal> animals) {
        writer.key("animals").array();
        animals.forEach(animal -> buildPerformer(writer, animal));
        writer.endArray();
    }

    /**
     * Uses call-by-reference to write an animal into intermediate result
     * of JSON writing.
     *
     * @param writer    instance of JSONWriter containing intermediate result
     *                  of JSON writing
     * @param animal    an instance of animal that will perform in an attraction
     */
    private static void buildPerformer(JSONWriter writer,
                                       Animal animal) {
        writer.object()
                .key("name").value(animal.getName())
                .endObject();
    }
}
