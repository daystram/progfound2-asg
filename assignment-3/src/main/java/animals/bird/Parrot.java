package animals.bird;

/**
 * A class to represent <i>parrot</i>.
 *
 * @see Bird
 */
public class Parrot extends Bird {

    /**
     * Constructs an instance of {@code Parrot}. The animal info parameter is
     * passed to the super class' constructor.
     *
     * @param info the array of animal info
     * @throws Exception if the super class' constructor fails parsing the special condition
     */
    public Parrot(String[] info) throws Exception {
        super(info);
    }
}
