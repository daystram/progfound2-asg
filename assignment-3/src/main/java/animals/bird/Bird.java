package animals.bird;

import animals.Animal;

/**
 * An abstract class to represent <i>mammals</i>. Is subclassed by the
 * different types of birds.
 *
 * @see Eagle
 * @see Parrot
 */
public abstract class Bird extends Animal {

    boolean layingEggs;     // bird egg laying status

    /**
     * Constructs a new {@code Bird} object.
     * Calls the super class' constructor.
     *
     * @param info the array of animal info
     * @throws Exception if the given special condition is other than {@code pregnant}
     */
    Bird(String[] info) throws Exception {
        super(info);
        switch (info[6]) {
            case "laying eggs":
                this.layingEggs = true;
                break;
            case "":
                this.layingEggs = false;
                break;
            default:
                throw new Exception();
        }
    }

    /**
     * Returns the state of {@code specialCondition} of the bird.
     * This is an implementation from {@code Animal}.
     * This returns {@code true} if the bird is not laying eggs, {@code false} otherwise.
     *
     * @return the state of the special condition
     * @see Animal
     */
    @Override
    protected boolean specialCondition() {
        return !layingEggs;
    }
}
