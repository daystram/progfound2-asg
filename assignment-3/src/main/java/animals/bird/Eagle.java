package animals.bird;

/**
 * A class to represent <i>eagle</i>.
 *
 * @see Bird
 */
public class Eagle extends Bird {

    /**
     * Constructs an instance of {@code Eagle}. The animal info parameter is
     * passed to the super class' constructor.
     *
     * @param info the array of animal info
     * @throws Exception if the super class' constructor fails parsing the special condition
     */
    public Eagle(String[] info) throws Exception {
        super(info);
    }
}
