package animals.factory;

import animals.Animal;
import animals.mammal.Cat;
import animals.mammal.Hamster;
import animals.mammal.Lion;
import animals.mammal.Whale;


/**
 * A <i>factory</i> class used to generate {@code Animal} of {@code Mammal} types.
 *
 * @see AnimalFactory
 * @see animals.mammal.Mammal
 * @see Animal
 */
class MammalFactory extends AnimalFactory {

    /**
     * Returns the instance of the animal based on its type of section {@code Mammal}.
     *
     * @param info the array of info regarding the animal
     * @return the generated {@code Animal} of type {@code Mammal} object
     * @throws Exception if info contains unlisted {@code Mammal} type
     * @see animals.mammal.Mammal
     */
    static Animal generate(String[] info) throws Exception {
        switch (type) {
            case "Hamster":
                return new Hamster(info);
            case "Lion":
                return new Lion(info);
            case "Cat":
                return new Cat(info);
            case "Whale":
                return new Whale(info);
            default:
                throw new Exception();
        }
    }
}
