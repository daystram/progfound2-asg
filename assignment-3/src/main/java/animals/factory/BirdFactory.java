package animals.factory;

import animals.Animal;
import animals.bird.Eagle;
import animals.bird.Parrot;

/**
 * A <i>factory</i> class used to generate {@code Animal} of {@code Bird} types.
 *
 * @see AnimalFactory
 * @see animals.bird.Bird
 * @see Animal
 */
class BirdFactory extends AnimalFactory {

    /**
     * Returns the instance of the animal based on its type of section {@code Bird}.
     *
     * @param info the array of info regarding the animal
     * @return the generated {@code Animal} of type {@code Bird} object
     * @throws Exception if info contains unlisted {@code Bird} type
     * @see animals.bird.Bird
     */
    static Animal generate(String[] info) throws Exception {
        switch (type) {
            case "Eagle":
                return new Eagle(info);
            case "Parrot":
                return new Parrot(info);
            default:
                throw new Exception();
        }
    }
}
