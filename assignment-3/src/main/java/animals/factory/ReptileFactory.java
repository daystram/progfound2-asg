package animals.factory;

import animals.Animal;
import animals.reptile.Snake;

/**
 * A <i>factory</i> class used to generate {@code Animal} of {@code Reptile} types.
 *
 * @see AnimalFactory
 * @see animals.reptile.Reptile
 * @see Animal
 *
 */
class ReptileFactory extends AnimalFactory {

    /**
     * Returns the instance of the animal based on its type of section {@code Reptile}.
     *
     * @param info the array of info regarding the animal
     * @return the generated {@code Animal} of type {@code Reptile} object
     * @throws Exception if info contains unlisted {@code Reptile} type
     * @see animals.reptile.Reptile
     */
    static Animal generate(String[] info) throws Exception {
        switch (type) {
            case "Snake":
                return new Snake(info);
            default:
                throw new Exception();
        }
    }
}
