package animals.factory;

import animals.Animal;
import java.util.Arrays;
import java.util.List;

/**
 * An abstract <i>factory</i> class used to easily generate {@link Animal} instances based on the
 * type contained in the given parameter. Uses the matching subclass, either {@link MammalFactory},
 * {@link BirdFactory}, or {@link ReptileFactory}, in order to do so. Is subclassed by helper
 * factories for the various animal types.
 *
 * @see MammalFactory
 * @see BirdFactory
 * @see ReptileFactory
 */

public abstract class AnimalFactory {
    /**
     * The {@code List} of registered Mammal types.
     * Must be modified when more Mammal types are added.
     *
     * @see animals.mammal.Mammal
     */
    public static final List<String> MAMMAL_TYPE = Arrays.asList("Hamster", "Lion", "Cat", "Whale");

    /**
     * The {@code List} of registered Bird types.
     * Must be modified when more Bird types are added.
     *
     * @see animals.bird.Bird
     */
    public static final List<String> BIRD_TYPE = Arrays.asList("Eagle", "Parrot");

    /**
     * The {@code List} of registered Reptile types.
     * Must be modified when more Reptile types are added.
     *
     * @see animals.reptile.Reptile
     */
    public static final List<String> REPTILE_TYPE = Arrays.asList("Snake");

    /**
     * The current animal type passed to this factory class.
     */
    protected static String type;

    /**
     * Returns {@code Animal} instances based on the type contained in the given info.
     * This method uses either {@link MammalFactory},
     * {@link BirdFactory}, or {@link ReptileFactory}.
     *
     * @param info the array of info regarding the animal
     * @return the generated {@code Animal} object
     * @throws Exception if info contains unlisted {@code Animal} type
     */
    public static Animal create(String[] info) throws Exception {
        // check if 'name' field is empty
        if (info[2].isEmpty()) {
            throw new Exception();
        }
        type = info[1];
        if (MAMMAL_TYPE.contains(type)) {
            return MammalFactory.generate(info);
        } else if (BIRD_TYPE.contains(type)) {
            return BirdFactory.generate(info);
        } else if (REPTILE_TYPE.contains(type)) {
            return ReptileFactory.generate(info);
        } else {
            throw new Exception();
        }
    }
}
