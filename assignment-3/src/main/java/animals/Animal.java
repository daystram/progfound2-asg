package animals;

/**
 * An abstract class to represent <i>animals</i>. Is subclassed by the different sections/types of
 * animals.
 *
 * @see animals.mammal.Mammal
 * @see animals.bird.Bird
 * @see animals.reptile.Reptile
 */
public abstract class Animal {

    private int id;             // animal ID, unique
    private String type;        // animal type
    private String name;        // animal name
    private Body body;          // animal body
    private Condition condition;// animal health condition

    /**
     * Constructs an instance of {@code Animal}.
     * The parameter is parsed here, exceptions will
     * be handled by the caller.
     *
     * @param info the array of info regarding the animal
     */
    public Animal(String[] info) {
        this.id = Integer.parseInt(info[0]);
        this.type = info[1];
        this.name = info[2];
        this.body = new Body(Double.parseDouble(info[4]), Double.parseDouble(info[5]),
                Gender.parseGender(info[3]));
        this.condition = Condition.parseCondition(info[7]);
    }

    /**
     * Returns the name.
     *
     * @return the animals's name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the ID (unique identification number).
     *
     * @return the animal's ID
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the type.
     *
     * @return the animal's type
     * @see animals.factory.AnimalFactory
     */
    public String getType() {
        return type;
    }

    /**
     * Returns the gender.
     *
     * @return {@code Gender} 0f the animal
     * @see Gender
     * @see Body
     */
    protected Gender getGender() {
        return body.getGender();
    }

    /**
     * Returns the health condition.
     *
     * @return the health condition
     * @see Condition
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * Returns the body length (in cm).
     *
     * @return the body length
     * @see Body
     */
    public double getLength() {
        return body.getLength();
    }

    /**
     * Returns the body weight (in Kg).
     *
     * @return the body weight
     * @see Body
     */
    public double getWeight() {
        return body.getWeight();
    }

    /**
     * Returns the legibility of the animal to do a performance.
     * Each animal types has different special conditions, but
     * has to be healthy to perform.
     *
     * @return the legibility of the animal to do a performance.
     */
    public boolean isLegible() {
        return condition == Condition.HEALTHY && specialCondition();
    }

    /**
     * Performs more specific checking to know whether an animal is able
     * to perform or not.
     *
     * @return special condition of the animal
     */
    protected abstract boolean specialCondition();
}
