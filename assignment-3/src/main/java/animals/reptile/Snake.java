package animals.reptile;

/**
 * A class to represent <i>snake</i>.
 *
 * @see Reptile
 */
public class Snake extends Reptile {

    /**
     * Constructs an instance of {@code Snake}. The animal info parameter is
     * passed to the super class' constructor.
     *
     * @param info the array of animal info
     * @throws Exception if the super class' constructor fails parsing the special condition
     */
    public Snake(String[] info) throws Exception {
        super(info);
    }
}
