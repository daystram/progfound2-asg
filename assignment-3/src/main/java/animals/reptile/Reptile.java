package animals.reptile;

import animals.Animal;

/**
 * An abstract class to represent <i>mammals</i>. Is subclassed by the
 * different types of mammals.
 *
 * @see Snake
 */
public abstract class Reptile extends Animal {

    boolean isTamed;        // reptile tameness status

    /**
     * Constructs a new {@code Reptile} object.
     * Calls the super class' constructor.
     *
     * @param info the array of animal info
     * @throws Exception if the given special condition is other than {@code tamed} or {@code wild}
     */
    Reptile(String[] info) throws Exception {
        super(info);
        switch (info[6]) {
            case "tame":
                this.isTamed = true;
                break;
            case "wild":
                this.isTamed = false;
                break;
            default:
                throw new Exception();
        }
    }

    /**
     * Returns the state of {@code specialCondition} of the reptile.
     * This is an implementation from {@code Animal}.
     * This returns {@code true} if the reptile is tame, {@code false} otherwise.
     *
     * @return the state of the special condition
     * @see Animal
     */
    @Override
    protected boolean specialCondition() {
        return isTamed;
    }
}

