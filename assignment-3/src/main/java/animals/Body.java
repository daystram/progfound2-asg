package animals;

/**
 * This class describes bodily features in an animal.
 *
 * @author Programming Foundations 2 Teaching Team
 */
class Body {

    private double length;  // body length in cm
    private double weight;  // body weight in Kg
    private Gender gender;  // animal gender

    /**
     * Constructs an instance of {@code Body} that part of an {@code Animal}.
     *
     * @param length length of animal in centimeters
     * @param weight weight of animal in kilograms
     * @param gender gender of animal (male/female)
     */
    Body(double length, double weight, Gender gender) {
        this.length = length;
        this.weight = weight;
        this.gender = gender;
    }

    /**
     * Returns the body length in cm.
     *
     * @return the body length
     */
    double getLength() {
        return length;
    }

    /**
     * Returns the body weight in Kg.
     *
     * @return the body weight
     */
    double getWeight() {
        return weight;
    }

    /**
     * Returns the gender.
     *
     * @return the gender
     */
    Gender getGender() {
        return gender;
    }
}
