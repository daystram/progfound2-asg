package animals.sections;

/**
 * A class to represent <i>mammal section</i>.
 * This class has a default name "Explore the Mammals"
 */
public class MammalSection extends Section {

    // section name
    private static final String SECTION_NAME = "Explore the Mammals";

    /**
     * Constructs an instance of {@code MammalSection}.
     * Passes the super class' constructor with the default section name.
     *
     * @see Section
     */
    public MammalSection() {
        super(SECTION_NAME);
    }
}
