package animals.sections;

/**
 * A class to represent <i>reptile section</i>.
 * This class has a default name "Reptilian Kingdom"
 */
public class ReptileSection extends Section {

    // section name
    private static final String SECTION_NAME = "Reptilian Kingdom";

    /**
     * Constructs an instance of {@code ReptileSection}.
     * Passes the super class' constructor with the default section name.
     *
     * @see Section
     */
    public ReptileSection() {
        super(SECTION_NAME);
    }
}
