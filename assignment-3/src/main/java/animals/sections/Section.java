package animals.sections;

import java.util.ArrayList;

/**
 * An abstract class to represent <i>section</i>. Is subclassed by the different
 * sections available.
 *
 * @see MammalSection
 * @see BirdSection
 * @see ReptileSection
 */
public abstract class Section {

    private String name;                // the name of the section
    private ArrayList<String> types;    // the types of animal contained in the section

    /**
     * Constructs an instance of {@code Section}.
     *
     * @param name the name of the section
     */
    Section(String name) {
        this.name = name;
        types = new ArrayList<>();  // initialize the ArrayList
    }

    /**
     * Adds more animal type to this section.
     *
     * @param type the type of animal to be added
     */
    public void addType(String type) {
        types.add(type);
    }

    /**
     * Returns {@code true} if the given type exists in this section,
     * {@code false} otherwise.
     *
     * @param type the animal type to be checked
     * @return the availability of the given animal type in this section
     */
    public boolean hasType(String type) {
        return types.contains(type);
    }

    /**
     * Returns the name of this section.
     *
     * @return the name of this section
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the {@code ArrayList} of the animals on this section.
     *
     * @return the {@code ArrayList} of the animals on this section
     */
    public ArrayList<String> getTypes() {
        return types;
    }

}
