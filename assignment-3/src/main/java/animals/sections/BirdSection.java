package animals.sections;

/**
 * A class to represent <i>bird section</i>.
 * This class has a default name "World of Aves"
 */
public class BirdSection extends Section {

    // section name
    private static final String SECTION_NAME = "World of Aves";

    /**
     * Constructs an instance of {@code BirdSection}.
     * Passes the super class' constructor with the default section name.
     *
     * @see Section
     */
    public BirdSection() {
        super(SECTION_NAME);
    }
}
