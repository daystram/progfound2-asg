package animals.mammal;

import animals.Gender;

/**
 * A class to represent <i>lion</i>.
 *
 * @see Mammal
 */
public class Lion extends Mammal {

    /**
     * Constructs an instance of {@code Lion}. The animal info parameter is
     * passed to the super class' constructor.
     *
     * @param info the array of animal info
     * @throws Exception if the super class' constructor fails parsing the special condition
     */
    public Lion(String[] info) throws Exception {
        super(info);
    }

    /**
     * Returns the state of {@code specialCondition} of the mammal.
     * This is an implementation from {@code Animal}.
     * This returns {@code true} if the mammal is pregnant, {@code false} otherwise.
     * {@code Lion} must be male, as an added special condition to be legible to perform.
     * 
     * @return the state of the special condition
     * @see animals.Animal
     */
    @Override
    protected boolean specialCondition() {
        return !isPregnant && getGender() == Gender.MALE;
    }
}
