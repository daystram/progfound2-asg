package animals.mammal;

/**
 * A class to represent <i>hamster</i>.
 *
 * @see Mammal
 */
public class Hamster extends Mammal {

    /**
     * Constructs an instance of {@code Hamster}. The animal info parameter is
     * passed to the super class' constructor.
     *
     * @param info the array of animal info
     * @throws Exception if the super class' constructor fails parsing the special condition
     */
    public Hamster(String[] info) throws Exception {
        super(info);

    }
}
