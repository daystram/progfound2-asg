package animals.mammal;

import animals.Animal;

/**
 * An abstract class to represent <i>mammals</i>. Is subclassed by the
 * different types of mammals.
 *
 * @see Lion
 * @see Cat
 * @see Hamster
 * @see Whale
 */
public abstract class Mammal extends Animal {

    boolean isPregnant;     // mammal pregnancy status

    /**
     * Constructs a new {@code Mammal} object.
     * Calls the super class' constructor.
     *
     * @param info the array of animal info
     * @throws Exception if the given special condition is other than {@code pregnant}
     */
    Mammal(String[] info) throws Exception {
        super(info);
        switch (info[6]) {
            case "pregnant":
                this.isPregnant = true;
                break;
            case "":
                this.isPregnant = false;
                break;
            default:
                throw new Exception();
        }
    }

    /**
     * Returns the state of {@code specialCondition} of the mammal.
     * This is an implementation from {@code Animal}.
     * This returns {@code true} if the mammal is pregnant, {@code false} otherwise.
     *
     * @return the state of the special condition
     * @see Animal
     */
    @Override
    protected boolean specialCondition() {
        return !isPregnant;
    }
}
