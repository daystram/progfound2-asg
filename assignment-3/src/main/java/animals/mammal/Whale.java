package animals.mammal;

/**
 * A class to represent <i>whale</i>.
 *
 * @see Mammal
 */
public class Whale extends Mammal {

    /**
     * Constructs an instance of {@code Whale}. The animal info parameter is
     * passed to the super class' constructor.
     *
     * @param info the array of animal info
     * @throws Exception if the super class' constructor fails parsing the special condition
     */
    public Whale(String[] info) throws Exception {
        super(info);
    }
}
