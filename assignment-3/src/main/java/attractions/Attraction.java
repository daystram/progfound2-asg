package attractions;

import animals.Animal;

import java.util.ArrayList;
import java.util.List;

/**
 * A class to represent <i>attraction</i>.
 * Implements {@code Showable} and {@code SelectedAttraction} to allow the attractions to
 * suit the required implementation as a 'showable' attraction, and a 'registrable' attraction
 * to be used in the writer class.
 *
 * @see Showable
 * @see SelectedAttraction
 * @see writer.RegistrationWriter
 */
public class Attraction implements Showable, SelectedAttraction {

    private String name;                    // the name of attraction
    private ArrayList<String> allowedTypes; // the animal types allowed to perform here
    private List<Animal> performer;         // the animals ready to perform
    private String selectedType;            // the selected animal type

    /**
     * Constructs an instance of {@code Attraction}.
     *
     * @param name the name of the attraction
     */
    public Attraction(String name) {
        this.name = name;
        this.allowedTypes = new ArrayList<>();  // initialize the ArrayList
    }

    /**
     * Returns the name of attraction.
     *
     * @return the name of attraction
     * @see Showable
     * @see SelectedAttraction
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Returns the animal type selected by the {@code Visitor}.
     *
     * @return the animal type selected
     * @see visitor.Visitor
     * @see SelectedAttraction
     */
    @Override
    public String getType() {
        return selectedType;
    }

    /**
     * Sets the animal type selected by the {@code Visitor}.
     *
     * @param type the animal type selected
     * @see visitor.Visitor
     * @see SelectedAttraction
     */
    @Override
    public void setType(String type) {
        this.selectedType = type;
    }

    /**
     * Returns the animals ready to perform in this attraction of the selected type.
     *
     * @return the animals ready to perform
     * @see SelectedAttraction
     */
    @Override
    public List<Animal> getPerformers() {
        List<Animal> result = new ArrayList<>();    // temporary storage
        for (Animal animal : performer) {
            if (animal.getType().equals(selectedType)) {
                result.add(animal);     // animal matches selected type, add to list
            }
        }
        return result;
    }

    /**
     * Adds the animal types allowed to perform in this attraction.
     *
     * @param type the animal type to be added
     * @see Showable
     */
    @Override
    public void addType(String type) {
        allowedTypes.add(type);
    }

    /**
     * Sets the animals ready to perform in this attraction.
     *
     * @param legibleAnim the animals ready to perform
     * @see Showable
     */
    @Override
    public void setPerformer(ArrayList<Animal> legibleAnim) {
        this.performer = legibleAnim;
    }

    /**
     * Returns the animal types allowed to perform in this attraction.
     *
     * @return the allowed animal types
     * @see Showable
     */
    @Override
    public List<String> getAllowedType() {
        return allowedTypes;
    }

    /**
     * Returns {@code true} if this attraction's allowed animal types contains the given type.
     *
     * @param type the animal type to be checked
     * @return the given type as the allowed animal types
     * @see Showable
     */
    @Override
    public boolean hasAllowedType(String type) {
        return allowedTypes.contains(type);
    }
}
