package attractions;

import animals.Animal;

import java.util.List;

/**
 * This interface describes expected behaviours for any type ((abstract)
 * class, interface) that represents the concept of attraction that
 * will be watched by a visitor in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @see visitor.Visitor
 */
public interface SelectedAttraction {

    /**
     * Returns the name of attraction.
     *
     * @return the name of attraction
     */
    String getName();

    /**
     * Returns the selected animal type from this attraction.
     *
     * @return the
     */
    String getType();

    /**
     * Sets the selected animal type from this attraction.
     *
     * @param selType the selected animal type
     */
    void setType(String selType);

    /**
     * Returns all performers of this attraction.
     *
     * @return all the performers of this attraction
     */
    List<Animal> getPerformers();
}
