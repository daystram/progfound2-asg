package attractions.factory;

import attractions.Attraction;

import java.util.Arrays;
import java.util.List;

/**
 * A <i>factory</i> class used to easily generate {@link Attraction} instances based on the names
 * contained in the given parameter. The available attraction are listed here, must be modified
 * to add more attractions.
 *
 * @see Attraction
 */
public class AttractionFactory {

    // the available attractions
    private static final List<String> AVAIL_ATTR = Arrays.asList("Circles of Fires",
            "Counting Masters", "Dancing Animals", "Passionate Coders");

    /**
     * Returns {@link Attraction} instances based on the name contained in the parameter.
     *
     * @param type the name of the new {@code Attraction}
     * @return the generated {@code Attraction} object
     * @throws Exception if info contains unlisted {@code Attraction}
     */
    public static Attraction create(String type) throws Exception {
        if (AVAIL_ATTR.contains(type)) {
            return new Attraction(type);
        } else {
            throw new Exception();
        }
    }
}
