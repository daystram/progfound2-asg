package attractions;

import animals.Animal;

import java.util.ArrayList;
import java.util.List;

/**
 * This interface describes expected behaviours for any type ((abstract)
 * class, interface) that represents the concept of attraction that
 * will be showed in Javari Park.
 */
public interface Showable {

    /**
     * Returns the name of the attraction.
     *
     * @return the name of the attraction
     */
    String getName();

    /**
     * Adds animal type which are allowed to perform in this attraction.
     *
     * @param type the animal type to be added
     */
    void addType(String type);

    /**
     * Returns the animal types allowed to perform in this attraction.
     *
     * @return the animal types allowed to perform
     */
    List<String> getAllowedType();

    /**
     * Returns {@code true} if the attraction contains the given type in the allowed animals.
     *
     * @param type the animal type to be checked
     * @return the availability of the given type
     */
    boolean hasAllowedType(String type);

    /**
     * Sets the animals ready to perform in this attraction.
     *
     * @param legibleAnim the animals ready to perform
     */
    void setPerformer(ArrayList<Animal> legibleAnim);
}
