package visitor;

import animals.sections.Section;
import attractions.SelectedAttraction;

import java.util.ArrayList;
import java.util.List;

/**
 * A class to represent <i>visitor</i>.
 * Also used to store the temporarily selected {@code Section}, {@code Animal},
 * and {@code Attraction}, before verified by the visitor.
 *
 * @see Section
 * @see animals.Animal
 * @see attractions.Attraction
 */
public class Visitor {

    private static int lastId = 0;              // ID of previous visitor instance

    private int id;                             // visitor ID
    private String name;                        // visitor name
    private List<SelectedAttraction> finalAttr; // final chosen attractions

    // temporary selections
    private Section selSection;             // selected section
    private String selType;                 // selected animal type
    private SelectedAttraction selAttr;     // selected attraction

    /**
     * Constructs a new instance of {@code Visitor}.
     * The ID given for this visitor will be 1+ from the previously
     * instantiated visitor, to ensure uniqueness.
     */
    public Visitor() {
        this.id = ++lastId;
        finalAttr = new ArrayList<>();      // initialize the ArrayList
    }

    /**
     * Returns the ID (unique) of this {@code Visitor}.
     *
     * @return the ID
     */
    public int getId() {
        return id;
    }

    /**
     * Set the name of this {@code Visitor}.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the name of this {@code Visitor}.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the temporarily selected {@code Section}.
     *
     * @param selSection the temporarily selected {@code Section}
     * @see Section
     */
    public void setSection(Section selSection) {
        this.selSection = selSection;
    }

    /**
     * Returns the temporarily selected {@code Section}.
     *
     * @return the temporarily selected {@code Section}
     * @see Section
     */
    public Section getSection() {
        return selSection;
    }

    /**
     * Sets the temporarily selected {@code Animal} type.
     *
     * @param selType the temporarily selected {@code Animal} type
     * @see animals.Animal
     */
    public void setSelType(String selType) {
        this.selType = selType;
    }

    /**
     * Returns the temporarily selected {@code Animal} type.
     *
     * @return the temporarily selected {@code Animal} type
     * @see animals.Animal
     */
    public String getSelType() {
        return selType;
    }

    /**
     * Sets the temporarily selected {@code Attraction}.
     *
     * @param selAttr the temporarily selected {@code Attraction}
     * @see attractions.Attraction
     */
    public void setAttr(SelectedAttraction selAttr) {
        this.selAttr = selAttr;
    }

    /**
     * Returns the temporarily selected {@code Attraction}.
     *
     * @return the temporarily selected {@code Attraction}
     * @see attractions.Attraction
     */
    public SelectedAttraction getAttr() {
        return selAttr;
    }

    /**
     * Sets the temporarily selected {@code Attraction} final.
     */
    public void setAttrFinal() {
        finalAttr.add(selAttr);
    }

    /**
     * Returns the final chosen {@code Attraction}.
     *
     * @return the chosen {@code Attraction}
     */
    public List<SelectedAttraction> getFinalAttr() {
        return finalAttr;
    }
}
