package reader;

import animals.factory.AnimalFactory;
import animals.sections.BirdSection;
import animals.sections.MammalSection;
import animals.sections.ReptileSection;
import animals.sections.Section;

import java.util.ArrayList;

/**
 * A helper class used to parse the {@link Section} from the CSV file
 * read in {@link CsvReader}.
 *
 * @see CsvReader
 * @see Section
 */
class SectionReader extends CsvReader {

    /**
     * Parses the {@link Section} from the read CSV files,
     * returning the array of {@link Section}.
     * This function also parses the categories/classification
     * of every animal types given.
     * This class has to be modified upon adding more {@link Section}.
     *
     * @return the array of created {@code Section}
     * @see Section
     */
    static Section[] parseSection() {
        ArrayList<Section> sectionIn = new ArrayList<>();   // temporary storage
        // initialize bad input line counter
        validAmount[0][0] = 0;
        validAmount[0][1] = 0;
        validAmount[2][0] = parsedFile[1].length;

        // iterate through all the read lines
        for (String[] line : parsedFile[1]) {
            try {
                String type = line[0];  // get the type entry
                Section active = null;  // allocate new Section
                if (AnimalFactory.MAMMAL_TYPE.contains(type)) {         // type is Mammal type
                    // check if section already created
                    for (Section section : sectionIn) {
                        if (section instanceof MammalSection) {
                            // section already created, use it instead
                            active = section;
                            break;
                        }
                    }
                    if (active == null) {
                        // section not yet created, create new
                        active = new MammalSection();
                        sectionIn.add(active);  // store new section
                        validAmount[0][0]++;    // parsed line is valid
                    }
                } else if (AnimalFactory.BIRD_TYPE.contains(type)) {    // type is Bird type
                    // check if section already created
                    for (Section section : sectionIn) {
                        if (section instanceof BirdSection) {
                            // section already created, use it instead
                            active = section;
                            break;
                        }
                    }
                    if (active == null) {
                        // section not yet created, create new
                        active = new BirdSection();
                        sectionIn.add(active);  // store new section
                        validAmount[0][0]++;    // parsed line is valid
                    }
                } else if (AnimalFactory.REPTILE_TYPE.contains(type)) { // type is Reptile type
                    // check if section already created
                    for (Section section : sectionIn) {
                        if (section instanceof ReptileSection) {
                            // section already created, use it instead
                            active = section;
                            break;
                        }
                    }
                    if (active == null) {
                        // section not yet created, create new
                        active = new ReptileSection();
                        sectionIn.add(active);  // store new section
                        validAmount[0][0]++;    // parsed line is valid
                    }
                } else {
                    throw new Exception("no section");  // Section given is unlisted
                }

                if (active.hasType(type)) {
                    throw new Exception("type in section"); // animal type is already added within
                } else {
                    active.addType(type);   // add new animal type to the section
                }
            } catch (Exception e) {
                if (e.getMessage().equals("no section")) {
                    // the given section is not listed
                    validAmount[0][1]++;    // parsed line is invalid
                } else if (e.getMessage().equals("type in section")) {
                    // type already contained in the Section
                    validAmount[2][0]--;    // parsed line is invalid
                    validAmount[2][1]++;
                }
            }
        }
        return sectionIn.toArray(new Section[0]);   // return the complete array of Sections
    }
}
