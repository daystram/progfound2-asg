package reader;

import animals.Animal;
import animals.factory.AnimalFactory;

import java.util.ArrayList;

/**
 * A helper class used to parse the {@link Animal} records from the CSV file
 * read in {@link CsvReader}.
 *
 * @see CsvReader
 * @see Animal
 */
class RecordReader extends CsvReader {

    /**
     * Parses the {@link Animal} from the read CSV files,
     * returning the array of {@link Animal}.
     *
     * @return the array of created {@code Animal}
     * @see Animal
     * @see AnimalFactory
     */
    static Animal[] parseRecord() {
        ArrayList<Animal> animalIn = new ArrayList<>();     // temporary storage
        // parse every line from the read CSV
        for (String[] line : parsedFile[2]) {
            try {
                // create new animal instance
                Animal newAnim = AnimalFactory.create(line);
                // check for same ID in registered animals
                for (Animal registered : animalIn) {
                    if (registered.getId() == newAnim.getId()) {
                        throw new Exception();  // same ID found
                    }
                }
                animalIn.add(newAnim);  // store the new animal to the list
                validAmount[3][0]++;    // parsed line is valid
            } catch (Exception e) {
                validAmount[3][1]++;    // parsed line is invalid
            }
        }
        return animalIn.toArray(new Animal[0]);     // return the complete array of Animals
    }
}
