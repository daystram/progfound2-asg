package reader;

import animals.Animal;
import attractions.Attraction;
import attractions.factory.AttractionFactory;

import java.util.ArrayList;

/**
 * A helper class used to parse the {@link Attraction} from the CSV file
 * read in {@link CsvReader}.
 *
 * @see CsvReader
 * @see Attraction
 */
class AttractionReader extends CsvReader {

    /**
     * Parses the {@link Attraction} from the read CSV files,
     * returning the array of {@link Attraction}.
     * This function also links the parsed {@link Animal}
     * to each of the {@link Attraction} created.
     *
     * @param animals the {@code Animal} to link
     * @return the array of created {@code Attraction}
     * @see Attraction
     * @see AttractionFactory
     */
    static Attraction[] parseAttraction(Animal[] animals) {
        ArrayList<Attraction> attractionIn = new ArrayList<>(); // temporary storage
        // parse every line from the read CSV
        for (String[] line : parsedFile[0]) {
            try {
                Attraction newAttr = null;      // allocate new attraction
                boolean exist = false;
                for (Attraction registered : attractionIn) {
                    if (registered.getName().equals(line[1])) {
                        // the attraction has previously been created, use it instead
                        newAttr = registered;
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    // the attraction has not been created, make new one instead
                    newAttr = AttractionFactory.create(line[1]);
                    attractionIn.add(newAttr);
                    validAmount[1][0]++;    // parsed line is valid
                }
                newAttr.addType(line[0]);   // add type of animal to the attraction
            } catch (Exception e) {
                validAmount[1][1]++;    // parsed line is invalid
            }
        }

        // iterate through all registered attractions
        for (Attraction registeredAttr : attractionIn) {
            ArrayList<Animal> legibleAnim = new ArrayList<>();  // temporary storage
            // iterate through all registered animal types in attraction
            for (String type : registeredAttr.getAllowedType()) {
                // iterate through all registered animals
                for (Animal animal : animals) {
                    if (animal.getType().equals(type) && animal.isLegible()) {
                        legibleAnim.add(animal);  // animal of matching type is legible, add to list
                    }
                }
            }
            registeredAttr.setPerformer(legibleAnim);   // place legible animals to the attraction
        }

        return attractionIn.toArray(new Attraction[0]); // return complete array of Attractions
    }

}
