package reader;

import animals.Animal;
import animals.sections.Section;
import attractions.Attraction;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * A class used as a helper to read CSV file inputs.
 * This class uses other helper classes, {@link SectionReader},
 * {@link AttractionReader}, and {@link RecordReader}, each of
 * them parsing 3 different CSV files.
 *
 * @see SectionReader
 * @see AttractionReader
 * @see RecordReader
 */
public class CsvReader {

    // the 3 CSV file names
    private static final String[] FILE_NAME = {"animals_attractions.csv",
                                               "animals_categories.csv",
                                               "animals_records.csv"};

    // the default directory of the CSV files
    private static String dataRoot = "./assignment-3/data/";

    // the 4 info to be parsed
    private static final String[] fileTypes = {"sections",
                                               "attractions",
                                               "animal categories",
                                               "animal records"};

    // the amount if valid and invalid lines in the CSVs
    static int[][] validAmount = new int[4][2];

    // the lines read from the CSVs
    static String[][][] parsedFile = new String[3][][];

    /**
     * Constructs an instance of {@code CsvReader}.
     * This constructor also executes a function to read all the 3 CSV files.
     */
    public CsvReader() {
        readFile();
    }

    // function to read all 3 CSV files, gets a new directory from user of files are missing
    private void readFile() {
        Scanner input = new Scanner(System.in);     // create new Scanner
        System.out.print("\n... Opening default section database from data. ... ");
        while (true) {
            try {
                // parse all 4 data files
                for (int i = 0; i < 3; i++) {
                    File file = new File(dataRoot + FILE_NAME[i]);
                    Scanner fileIn = new Scanner(file);
                    // read all the lines in the CSV, splitted into each 'tokens'
                    ArrayList<String[]> temp = new ArrayList<>();
                    while (fileIn.hasNextLine()) {
                        temp.add(fileIn.nextLine().split(","));
                    }
                    parsedFile[i] = temp.toArray(new String[0][]);       // store read lines
                    fileIn.close();
                }
                break;
            } catch (Exception e) {
                if (e instanceof FileNotFoundException) {
                    // file not found
                    System.out.println("File not found or incorrect file!");
                    while (true) {
                        System.out.print("\nPlease provide the source data path: ");
                        dataRoot = input.nextLine();   // get new directory from user
                        if (dataRoot.isEmpty()) {
                            // user gives no input
                            System.out.println("Please enter a file directory!");
                            continue;
                        }
                        if (!dataRoot.substring(dataRoot.length() - 1).equals("/")) {
                            // add the slash symbol if not yet
                            dataRoot += "/";
                        }
                        break;
                    }
                } else {
                    // an uncaught exception has occurred, exit program
                    System.out.println("An error has occurred");
                    System.exit(0);
                }
                System.out.print("\n... Loading... ");
            }
        }
        System.out.print("Success... ");
    }

    /**
     * Returns the array of {@link Section} created from parsing the Sections CSV file.
     *
     * @return the array of {@code Section}
     * @see Section
     * @see SectionReader
     */
    public Section[] readSection() {
        return SectionReader.parseSection();
    }

    /**
     * Returns the array of {@link Attraction} created from parsing the Attractions CSV file.
     * This function also takes the {@link Animal} which has been parsed previously,
     * integrating them with the newly created {@link Attraction}.
     *
     * @param animals the array of {@code Animals}
     * @return the array of {@code Attraction}
     */
    public Attraction[] readAttraction(Animal[] animals) {
        return AttractionReader.parseAttraction(animals);
    }

    /**
     * Returns the array of {@link Animal} created from parsing the Records CSV file.
     *
     * @return the array of {@code Animal}
     */
    public Animal[] readRecord() {
        return RecordReader.parseRecord();
    }

    /**
     * Prints out the amount of valid and invalid lines from parsing the CSV files.
     */
    public void showValidAmount() {
        for (int i = 0; i < fileTypes.length; i++) {
            System.out.println("Found " + validAmount[i][0] + " valid " + fileTypes[i]
                    + " and " + validAmount[i][1] + " invalid " + fileTypes[i]);
        }
    }

}
