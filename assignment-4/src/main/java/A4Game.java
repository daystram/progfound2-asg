import game.GameGui;

import game.gui.exceptions.UnevenGridException;

/**
 * Main class where the main game runs.
 * Utilizes {@link GameGui} to display the game GUI.
 * Contains the action handlers for the button slicks on the GUI.
 * @see GameGui
 * @author Danny August Ramaputra
 */
public class A4Game {

    /**
     * Starts the memory game.
     * @param args program arguments
     */
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(() -> {
            try {
                // initialize game GUI with 6 x 6 memory grid
                new GameGui(10, 10);
            } catch (UnevenGridException e) {
                e.printStackTrace();
            }
        });
    }
}
