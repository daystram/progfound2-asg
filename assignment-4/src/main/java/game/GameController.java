package game;

import game.gui.AnimalButton;

import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 * A class used as a controller for Javari Memory Game.
 * Controls the logic for the game displayed on {@link GameGui}.
 * Action listeners are located here.
 * Implements {@link MemoryGame}, to suit the requirements of the game GUI.
 * @see GameGui
 * @see MemoryGame
 * @author Danny August Ramaputra
 */
class GameController implements MemoryGame {

    /** The game GUI to be controlled. */
    private final GameGui gameGui;

    /** The number of attempts. */
    private static int attempts;

    /** The number of tiles matched. */
    private static int matched;

    /** The last selected AnimalButton. */
    private static AnimalButton lastSelected;

    /** The state of being in delay. */
    private static boolean inDelay;

    /**
     * Creates a new instance of the {@code GameController}.
     * @param gameGui the game GUI to control
     */
    GameController(GameGui gameGui) {
        this.gameGui = gameGui; // attach to the game GUI
        attempts = 0;           // initialize the number of attempts and matched tiles
        matched = 0;
    }

    /**
     * Routine when an AnimalButton is clicked on the grid on the GUI.
     * An implementation from {@link MemoryGame}
     * @param clicked the AnimalButton clicked
     * @see GameGui
     * @see MemoryGame
     */
    public void clickAnimal(AnimalButton clicked) {
        if (!inDelay) {     // take action if not in delay
            clicked.cover(false);       // reveal clicked button
            if (lastSelected == null) {             // first button clicked
                lastSelected = clicked;
            } else if (lastSelected != clicked) {   // second button clicked
                gameGui.showAttempt(++attempts);   // update showed attempt
                // new timer action, for delay of 1s
                Timer timer = new Timer(1000, e -> {
                    if (lastSelected.getType() == clicked.getType()) {
                        // animals matched, hide both buttons
                        matched++;
                        lastSelected.setInvisible();
                        clicked.setInvisible();
                        checkEndGame();     // check if game end reached
                    } else {
                        // animals does not match, cover both buttons
                        clicked.cover(true);
                        lastSelected.cover(true);
                    }
                    lastSelected = null;
                    inDelay = false;        // no longer in delay
                });
                inDelay = true; // currently in delay
                timer.setRepeats(false);
                timer.start(); // do action
            }
        }
    }

    /**
     * Routine when Repeat button is clicked on the GUI.
     * Restarts the game.
     * An implementation from {@link MemoryGame}
     * @see MemoryGame
     */
    public void repeatGame() {
        attempts = 0;
        matched = 0;
    }

    /**
     * Routine when Exit button is clicked on the GUI.
     * Exits the game.
     * An implementation from {@link MemoryGame}
     * @see MemoryGame
     */
    public void exitGame() {
        System.exit(0);
    }

    /**
     * Checks if the game has ended (i.e. all animals matched).
     * Shows a popup with the number of attempts.
     */
    private void checkEndGame() {
        if (matched == (gameGui.row * gameGui.col) / 2) {
            JOptionPane.showMessageDialog(gameGui.getFrame(),
                                "Well Done! Completed in " + attempts + " tries",
                                    "Well Done!",
                                        JOptionPane.INFORMATION_MESSAGE);
            repeatGame();
            gameGui.reset();
        }
    }
}
