package game;

import game.gui.AnimalButton;
import game.gui.AnimalType;
import game.gui.exceptions.UnevenGridException;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 * A class to represent the Javari Memory Game GUI.
 * Displays the grid of the memory tiles. There are 18 different animals to match.
 * There may be double pairs if game is started with grid larger than 6 x 6.
 * Uses {@link MemoryGame} to control the game flow.
 * @see MemoryGame
 * @author Danny August Ramaputra
 */
public class GameGui {

    /** The JFrame object. */
    private final JFrame frame;

    /** The amount of rows on the game grid. */
    final int row;

    /** The amount of columns on the game grid. */
    final int col;

    /** The grid button size. */
    private static final int BUTTON_SIZE = 128;

    /** The game system to control events on the GUI. */
    private final MemoryGame controller;

    /** The memory game button grid. */
    private JPanel grid;

    /** The label showing number of attempts. */
    private JLabel attemptLabel;

    /**
     * Creates a new instance of {@code GameGui}.
     * Initializes the GUI for the memory game.
     * For games with grid larger than 6 x 6, there will be
     * double pairs.
     * @param row the number of memory grid rows
     * @param col the number of memory grid columns
     * @throws UnevenGridException if amount of cells in grid is odd
     */
    public GameGui(int row, int col) throws UnevenGridException {
        frame = new JFrame("Javari Memory Game");    // initialize frame

        if ((row * col) % 2 != 0) {
            throw new UnevenGridException();    // there is an odd number of cells
        }
        this.row = row;     // set the number of rows and columns
        this.col = col;

        controller = new GameController(this);  // create new controller for this game GUI

        //frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        initButtonGrid();           // initialize the button grid
        initFunctionButtons();      // initialize the function buttons
        initAttemptText();          // initialize the attempt counter text

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);      // show frame
    }

    /**
     * Initializes the button grid of the memory game.
     * Randomizes the position of the animals.
     * @see AnimalButton
     * @see AnimalType
     */
    private void initButtonGrid() {
        grid = new JPanel(new GridLayout(row, col));    // new GridLayout of the set size
        ArrayList<Integer> randInt = new ArrayList<>();
        for (int i = 0; i < (row * col) / 2; i++) {
            randInt.add(i);             // store number pairs in ArrayList
            randInt.add(i);
        }
        Collections.shuffle(randInt);   // shuffle
        for (int i = 0; i < row * col; i++) {
            // create instances of AnimalButtons to fill the grid
            AnimalButton button = new AnimalButton(
                                    AnimalType.parseType(randInt.get(i) % 18), BUTTON_SIZE);
            // attach action listener to the main class
            button.addActionListener(e -> controller.clickAnimal((AnimalButton) e.getSource()));
            grid.add(button);           // place button on grid
        }
        frame.getContentPane().add(grid, BorderLayout.PAGE_START);   // place panel on frame
    }

    /**
     * Initializes the function buttons.
     * The function buttons are 'Play Again?' and 'Exit'.
     */
    private void initFunctionButtons() {
        JPanel functions = new JPanel(new FlowLayout());
        JButton repeatBtn = new JButton("Play Again?"); // create 'Play Again?' button
        repeatBtn.setMnemonic('A');
        repeatBtn.addActionListener(e -> {
            controller.repeatGame();
            reset();
        });
        functions.add(repeatBtn);   // place on panel
        JButton exitBtn = new JButton("Exit");          // create 'Exit' button
        exitBtn.setMnemonic('x');
        exitBtn.addActionListener(e -> controller.exitGame());
        functions.add(exitBtn);     //place on panel
        frame.getContentPane().add(functions, BorderLayout.CENTER);  // place panel on frame
    }

    /**
     * Initializes the label showing number of attempts.
     */
    private void initAttemptText() {
        attemptLabel = new JLabel("", SwingConstants.CENTER);   // create label
        showAttempt(0);     // initialize displayed text
        frame.getContentPane().add(attemptLabel, BorderLayout.PAGE_END); // place label on frame
    }

    /**
     * Displays the number of attempts on a label.
     * @param attempt the number of attempts
     */
    void showAttempt(int attempt) {
        attemptLabel.setText("Number of Tries: " + attempt);
    }

    /**
     * Resets the memory grid.
     */
    void reset() {
        frame.remove(grid);      // remove the button grid
        initButtonGrid();       // reinitialize the button grid
        showAttempt(0);         // reset the displayed number of attempts
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    /**
     * Returns the JFrame of the GUI.
     * @return the frame
     */
    JFrame getFrame() {
        return frame;
    }
}
