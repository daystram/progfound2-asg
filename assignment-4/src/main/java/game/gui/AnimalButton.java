package game.gui;

import java.awt.Dimension;
import java.awt.Image;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * A class used to represent Buttons for the grid on the memory game.
 * Each button represents an animal type.
 * Extends {@link JButton} from the standard library.
 * @see JButton
 * @see AnimalType
 * @author Danny August Ramaputra
 */
public class AnimalButton extends JButton {

    /** Type of the animal showed by the button. */
    private AnimalType type;

    /** The image of the covered button. */
    private ImageIcon coverIcon;

    /** The image of the uncovered button (i.e. the animal). */
    private ImageIcon animalIcon;

    /** The base directory of the resources folder. */
    private static final String RESOURCES_DIR = "./assignment-4/resources/";

    /**
     * Creates a new instance of {@code AnimalButton}.
     * @param type the type of animal assigned to be button
     * @param buttonSize the size of the button
     * @see AnimalType
     */
    public AnimalButton(AnimalType type, int buttonSize) {
        super();    // initiate JButton
        this.setPreferredSize(new Dimension(buttonSize, buttonSize));  // set the button size
        this.setFocusPainted(false);    // disable outline on clicked button
        try {
            // try getting the icon for covered button
            this.coverIcon = scaleIcon(new ImageIcon(ImageIO.read(new File(
                    RESOURCES_DIR + "game/gui/logo/logo.png"))), buttonSize);
        } catch (IOException e) {
            // logo is not found
            System.err.println("logo.png cannot be found!");
            this.coverIcon = null;
        }
        try {
            // try getting the icon for the assigned animal type
            this.animalIcon = scaleIcon(new ImageIcon(ImageIO.read(new File(
                    RESOURCES_DIR + "game/gui/animals/" + type + ".png"))), buttonSize);
        } catch (IOException e) {
            // image not found
            System.err.println(type + ".png cannot be found!");
            this.animalIcon = null;
        }
        this.type = type;
        this.cover(true);
    }

    /**
     * Scales the image to 0.9 of the button size.
     * @param image the image to scale
     * @param buttonSize the size of the button
     * @return the scaled image
     */
    private ImageIcon scaleIcon(ImageIcon image, int buttonSize) {
        return new ImageIcon(image.getImage()
                                .getScaledInstance((int) (buttonSize * 0.9),
                                                    (int) (buttonSize * 0.9),
                                                    Image.SCALE_SMOOTH));
    }

    /**
     * Covers or reveals the animal image.
     * If cover image was not found, "HIDDEN" is shown.
     * If animal image was not found, the animal image will be shown instead.
     * @param cover covers the button if {@code true}, reveals otherwise
     */
    public void cover(boolean cover) {
        this.setText(null);     // remove shown text
        this.setIcon(null);     // remove shown image
        if (cover) {
            // cover button
            if (coverIcon == null) {
                this.setText("HIDDEN");         // image not found
            } else {
                this.setIcon(coverIcon);        // image found
            }
        } else {
            // reveal button
            if (animalIcon == null) {
                this.setText(type.toString());  // image not found
            } else {
                this.setIcon(animalIcon);       // image found
            }
        }
    }

    /**
     * Hides the button, used when the match has been found.
     */
    public void setInvisible() {
        this.setVisible(false);
    }

    /**
     * Returns the animal type the button is representing.
     * @return the animal type
     * @see AnimalType
     */
    public AnimalType getType() {
        return type;
    }
}
