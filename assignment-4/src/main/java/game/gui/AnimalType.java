package game.gui;

/**
 * An enumerator class used to describe the animal types available.
 * There are 18 different animal types.
 * @author Danny August Ramaputra
 */
public enum AnimalType {
    // the 18 animal types
    CAT, DOG, RABBIT, SNAKE, LADYBUG, SHEEP,
    PARROT, PIG, WHALE, ELEPHANT, FISH, COW,
    LION, TURTLE, SNAIL, MOUSE, CHICKEN, DUCK;

    /** The animal types placed in an array. */
    private static AnimalType[] types = {CAT, DOG, RABBIT, SNAKE, LADYBUG, SHEEP,
                                            PARROT, PIG, WHALE, ELEPHANT, FISH, COW,
                                            LION, TURTLE, SNAIL, MOUSE, CHICKEN, DUCK};

    /**
     * Returns the animal type based on the ID given.
     * @param num the ID of the animal
     * @return the animal type
     */
    public static AnimalType parseType(int num) {
        return types[num];
    }
}
