package game.gui.exceptions;

/**
 * An class to represent an exception.
 * It is when there is an odd number of buttons/cells on the memory game grid.
 */
public class UnevenGridException extends Exception {
    /**
     * Creates a new instance of {@code UnevenGridException}.
     */
    public UnevenGridException() {
        super("There is an odd number of cells in grid");
    }
}
