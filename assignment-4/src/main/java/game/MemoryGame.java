package game;

import game.gui.AnimalButton;

/**
 * An interface identifying the methods to implement for a memory game controller.
 * @author Danny August Ramaputra
 */
interface MemoryGame {

    /** Action if an AnimalButton is clicked on the grid. */
    void clickAnimal(AnimalButton button);

    /** Action if the 'Play Again?' button is clicked. */
    void repeatGame();

    /** Action if the 'Exit' button is clicked. */
    void exitGame();
}
