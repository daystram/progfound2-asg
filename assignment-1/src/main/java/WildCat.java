public class WildCat {

    // initialize variables
    String name;    // cat name
    double weight;  // In kilograms
    double length;  // In centimeters

    // define constructor
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    // define computeMassIndex() method
    public double computeMassIndex() {
        // calculate BMI
        return weight / Math.pow((length / 100), 2);
    }
}
