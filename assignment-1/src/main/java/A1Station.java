import java.util.Scanner;

public class A1Station {

    // initialize constant and variables
    private static final double THRESHOLD = 250; // in kilograms
    static int activeCat = 0;       // amount of undeparted cats
    static TrainCar currentTrain;   // frontmost train carriage

    // define main() method
    public static void main(String[] args) {
        // create Scanner object, and get cat amount
        Scanner input = new Scanner(System.in);
        int catAmount = Integer.parseInt(input.nextLine());

        // loop through all incoming cats
        for (int i = 0; i < catAmount; i++) {
            WildCat newCat;     // new incoming cat
            // get correct user input for new cat data
            while (true) {
                String[] newData = input.nextLine().split(",");     // cat information
                try {
                    // parse given info, create newCat instance
                    newCat = new WildCat(newData[0], Integer.parseInt(newData[1]),
                                                Integer.parseInt(newData[2]));
                    activeCat++;
                    break;
                } catch (Exception e) {
                    // inform user for invalid input
                    System.out.println("Invalid input!");
                }
            }

            // check if there are any undeparted carriages
            if (currentTrain == null) {
                // create new carriage
                currentTrain = new TrainCar(newCat);
            } else {
                // link the new carriage to existing one
                currentTrain = new TrainCar(newCat, currentTrain);
            }

            // check if current locomotive is above weight treshold
            if (currentTrain.computeTotalWeight() >= THRESHOLD) {
                // treshold exceeded, depart locomotive
                departTrain();
                currentTrain = null;
            }
        }
        // depart remaining locomotive if exists
        if (currentTrain != null) {
            departTrain();
        }
    }

    // define departTrain() method
    private static void departTrain() {
        // print locomotive departure info
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        currentTrain.printCar();        // print locomotive chain diagram
        // calculate average cat BMI in locomotive
        double averageBmi = currentTrain.computeTotalMassIndex() / activeCat;
        System.out.printf("Average mass index of all cats: %.2f\n", averageBmi);

        // categorize locomotive based on avg. BMI
        String category;
        if (averageBmi < 18.5) {
            category = "*underweight*";
        } else if (averageBmi < 25) {
            category = "*normal*";
        } else if (averageBmi < 30) {
            category = "*overweight*";
        } else {
            category = "*obese*";
        }

        // print category info
        System.out.println("In average, the cats in the train are " + category);
        activeCat = 0;      // reset amount of undeparter cats
    }
}
