public class TrainCar {

    // initialize constant and variables
    public static final double EMPTY_WEIGHT = 20; // In kilograms
    WildCat cat;        // cat within carriage
    TrainCar next;      // next linked carriage

    // define constructor; @param: WildCat cat
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    // define constructor; @param: WildCat cat, TrainCar next
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    // define cumputeTotalWeight() method
    public double computeTotalWeight() {
        // calculate toatl weight recursively
        if (this.next == null) {
            return EMPTY_WEIGHT + cat.weight;
        } else {
            return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        // calculate total BMI recursively
        if (this.next == null) {
            return cat.computeMassIndex();
        } else {
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        // print locomotive chain diagram recursively
        if (this.next == null) {
            System.out.println("(" + cat.name + ")");
        } else {
            System.out.print("(" + cat.name + ")--");
            next.printCar();
        }
    }
}
